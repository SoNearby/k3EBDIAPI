<?php
//header("Content-type: text/html; charset=utf-8");
/* 
 * 金蝶 wise 14.3 web soap 接口工具类
 */

class k3Web{
    
    public $host;
    public $companyName;
    public $AisID;//账套ID
    public $SoapClient;
    
    
    public function __construct($host,$companyName) {
        $this->host = $host;
        $this->companyName = iconv('gbk', 'UTF-8', $companyName);
        $this->getAisID();
    }
    
    //获取账套ID or -1
    public function getAisID(){
        $http = 'http://'.$this->host.'/KDWEBSERVICE/public.asmx?wsdl';//拼接soap服务地址
        $this->SoapClient = new SoapClient($http);//实例化soap客户端
        $res = $this->SoapClient->AisQuery();//执行soap服务提供的方法
        
        //如果账套数量大于1 返回 对象数组，如果只有一个账套返回对象。
        $zhangTao = $res->AisQueryResult->AisInfo;
        
        if(is_object($zhangTao)){
            if($zhangTao->AisName == $this->companyName){
                $this->AisID = $zhangTao->AisID;
            } else {
                $this->AisID = -1;
            }
            return;
        }
       
        //遍历对象数组，提取数组中的对象 匹配账套名称
        for($i=0;$i<count($zhangTao);$i++){
            $temp = $zhangTao[$i];
            if($temp->AisName == $this->companyName){
                $AisID = $temp->AisID;
            }
        }
        $this->AisID = $AisID ? $AisID : -1;//查询到账套号返回 or -1
    }
    
    //传入soap 服务模块 返回sopaClient 对象
    public function getSoapClient($model){
        $http = 'http://'.$this->host.'/KDWEBSERVICE/'.$model.'.asmx?wsdl';//拼接soap服务地址
        return new SoapClient($http);
    }    
}

/*
 * 
$k3 = new k3Web('192.168.200.131','杭州长迈信息技术有限公司');
//获取账套ID
$AisID = $k3->AisID;
//获取模块soap对象
$sop = $k3->getSoapClient('Voucher');
//执行soap服务对象的方法
$sop->QueryVoucher();
 *
 */