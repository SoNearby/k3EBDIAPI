<?php

/* 版本：V1 2018-03-25
 * K3 版本 14.3  EDBI接口工具
 * 参考文档：http://k3Server/K3api/Default.aspx?acct=)%20%20F%20%22%2C%20%2CP%20T%20%238%20*P!D%20%26D%2080!N%20%26@%20%3C0%20C%20%27%3C%20%3A%20!M%20%264%20)0%20S%20%20&language=chs
 */

class K3API{
    
    public $Token;
    public $host;
    public $voucherUpdateUrl;
    public $voucherQueryUrl;

    //对象初始化 获取token、初始化凭证查询、更新api地址
    function __construct($host,$authCode) {
        $url = "http://".$host."/K3API/Token/Create/?authorityCode=".$authCode."&language=CHS ";
        $res = $this->curl($url);
        if(!$res){
            exit('无法连接API接口!');;
        }
        $ob = json_decode($res);
        if($ob->StatusCode != 200){
            exit('授权码错误!');
        }
        $token = $ob->Data->Token;
        $this->Token = $token;
        $this->voucherQueryUrl = "http://".$host."/K3API/VoucherData/QueryVoucher?token=".$token;
        $this->voucherUpdateUrl = "http://".$host."/K3API/VoucherData/UpdateVoucher?token=".$token;
    }
    
    //curl 封装
    function curl($url,$method='get',$parameter=null){
        $object = curl_init();
        curl_setopt($object, CURLOPT_SSL_VERIFYPEER, false);//证书验证
        curl_setopt($object, CURLOPT_SSL_VERIFYHOST, 2);//证书验证
        curl_setopt($object, CURLOPT_RETURNTRANSFER,1);//以字符串形式返回请求结果
        curl_setopt($object, CURLOPT_TIMEOUT,15);//最长执行时间
        curl_setopt($object, CURLOPT_URL,$url);//设置要访问的url地址
        //curl_setopt($object, CURLOPT_HEADER, true);//是否显示请求响应头，一般用于排错
        
        if($method == 'post'){
            curl_setopt($object, CURLOPT_POST, 1);//请求方法post,请求的参数必须是编译后的json
            $parameter = json_encode($parameter);
            if(!$parameter){return false;}
            $header = array('Content-Type: application/json','Content-Length:'.strlen($parameter));//初始化请求头
            curl_setopt($object, CURLOPT_HTTPHEADER,$header);//设置请求头
            curl_setopt($object, CURLOPT_POSTFIELDS, $parameter);//设置请求参数
        }else{
            curl_setopt($object, CURLOPT_POST, 0);//请求方法get
        }
        //$httpCode = curl_getinfo($object, CURLINFO_HTTP_CODE);//判断是否成功请求url
        return curl_exec($object);
    }
    
    //凭证新增数据模型
       
}

/* 使用方法
$uthorityCode = '3cde8080067f7f26a6bcd4f60bce0758c5fd44ce00c6bc01';
$host = '127.0.0.1';
$k3 = new K3API($host,$uthorityCode);
$token = $k3->Token;

$url = "http://127.0.0.1/K3API/VoucherData/UpdateVoucher?token=$token";

$parameter = array();
$parameter['Filter'] = iconv('gbk','utf-8','汉字');

$res = $k3->curl($url, 'post', $parameter);

$StatusCode = $ob->StatusCode;//200 http 请求成功
$Message = $ob->Message;//Successful接口请求成功 

$ob = json_decode($res);

*/







