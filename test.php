<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'k3Api.class.php';

header("Content-type:text/html;charset=utf-8");

$uthorityCode = '3cde8080067f7f26a6bcd4f60bce0758c5fd44ce00c6bc01';
$host = '127.0.0.1';

$k3 = new K3API($host,$uthorityCode);

$EntriesTemp = array();//凭证分录明细数组
    $EntriesTemp['FEntryID']='1';//分录ID
    $EntriesTemp['FDC ']='1';//借贷方向 1:借方 0:贷方 true
    $EntriesTemp['FAccountName'] = '库存现金';//科目名称  true
    $EntriesTemp['FAccountNumber ']='1001';//科目代码 true
    $EntriesTemp['FAmount ']='5000';//本位币金额 true
    $EntriesTemp['FAmountFor ']='5000';//原币金额 ture;
    $EntriesTemp['FQuantity ']='5000';//数量
    $EntriesTemp['FUnitPrice ']='1';//单价
    $EntriesTemp['FExchangeRate ']='1';//汇率
    $EntriesTemp['FCurrencyName ']='人民币'; //币别名称 ture
    $EntriesTemp['FCurrencyNumber ']='RMB';//币别编码 true
    //可为空值
    $EntriesTemp['FExplanation ']='test';//备注
    $EntriesTemp['FMeasureUnit ']='';//单位
    $EntriesTemp['FMeasureUnitUUID']='';//单位UUID
    $EntriesTemp['FSettleNo ']='';//结算号
    $EntriesTemp['FSettleTypeName ']='';//结算类型
    $EntriesTemp['FTransNo '] = '业务号';//
    
$Entries = array();//凭证分录 二维数组
$Entries[] = $EntriesTemp;
    $EntriesTemp['FEntryID']='2';//分录ID
    $EntriesTemp['FDC ']='0';//借贷方向 1:借方 0:贷方 true
    $EntriesTemp['FAccountName'] = '兴业银行人民币户205849';//科目名称  true
    $EntriesTemp['FAccountNumber ']='1002.03';//科目代码 true
    $EntriesTemp['FAmount ']='5000';//本位币金额 true
    $EntriesTemp['FAmountFor ']='5000';//原币金额 ture;
    $EntriesTemp['FQuantity ']='5000';//数量
    $EntriesTemp['FUnitPrice ']='1';//单价
    $EntriesTemp['FExchangeRate ']='1';//汇率
    $EntriesTemp['FCurrencyName ']='人民币'; //币别名称 ture
    $EntriesTemp['FCurrencyNumber ']='RMB';//币别编码 true
    $EntriesTemp['FExplanation ']='';//备注
$Entries[] = $EntriesTemp;

$Voucher = array(); //凭证主体
$VoucherData = array();//凭证主体数据
    
    $VoucherData['Entries'] = $Entries;//分录 二维数组
    //$VoucherData['CashFlow'] = $CashFlow;//现金流量表 二维数组
    $VoucherData['FAttachments'] = '0';//附件张数 ture 必录
    $VoucherData['FCashier'] = '';//出纳 
    $VoucherData['FDate'] = '2018-04-25';//凭证日期 ture
    $VoucherData['FExplanation'] = 'test z';//备注
    $VoucherData['FGroup'] = '记';//凭证字 ture
    $VoucherData['FHandler'] = '';//会计主管
    $VoucherData['FNumber'] = '0';//凭证号 int  ture 
    $VoucherData['FPeriod'] = '4';// 会计期间 int  ture 
    $VoucherData['FPoster'] = '';//记账人
    $VoucherData['FPreparer '] = 'Administrator';//制单人 true
    $VoucherData['FSerialNum'] = '';//凭证序号 
    $VoucherData['FReference'] = '';//参考信息
    $VoucherData['FTransDate'] = date('Y-m-d H:i:s',time());//业务日期  true
    $VoucherData['FVoucherID'] = '0';//凭证Id 新增传0 修改传凭证Id ture
    $VoucherData['FYear'] = '2018';//会计年度 ture
    
    //凭证组装完成
    $Voucher['Replace'] = 'false';//只新增不修改
    $Voucher['VoucherData'] = $VoucherData;


//echo json_encode($Voucher);
//return;

$res = $k3->curl($k3->voucherUpdateUrl, 'post', $Voucher);

var_dump($res);


