<?php


header("Content-type:text/html;charset=utf-8");
class voucher{
    
    public $status = false;
    public $Entries = array();//凭证分录明细
    public $VoucherData = array();
    
    public $msg = '';
    public $son = array();//凭证实体
    public $Replace =  false;//只新增不修改
   
    
    
    //检查凭证分录项目合法性
    public  function checkEntries(){
        $Entries = $this->Entries;
        if( count($Entries) < 2){
            $this->msg .= "凭证分录数目小于2；";
        }
        $fdc_1 = $fdc_0 = 0;
        foreach($Entries as $entrie){
            //数量判定
            if(count($entrie) < 11){
               $this->msg .= "凭证分录缺少必填字段；"; 
            }
            //空值判定
            foreach($entrie as $k=>$v){
                if($v == null){
                    $this->msg .= "凭证分录必填字段[$k]为空；";
                }
            }
        }
    }
    
    //组装凭证
    public function make(){
        $this->checkEntries();
        $this->VoucherData['Entries'] = $this->Entries;
        //
        
        //最外层组装
        $this->son['Replace'] = $this->Replace;
        $this->son['VoucherData'] = $this->VoucherData;
        
        
    }
}

$Voucher = new voucher();

$EntriesTemp = array();//凭证分录明细数组
    $EntriesTemp['FEntryID']='1';//分录ID
    $EntriesTemp['FAccountName'] = '银行存款';//科目名称  true
    $EntriesTemp['FAccountNumber ']='';//科目代码 true
    $EntriesTemp['FAmount ']='';//本位币金额 true
    $EntriesTemp['FAmountFor ']='1';//原币金额 ture;
    $EntriesTemp['FCurrencyName ']='1'; //币别名称 ture
    $EntriesTemp['FCurrencyNumber ']='1';//币别编码 true
    $EntriesTemp['FDC ']='1';//借贷方向 1:借方 0:贷方 true
    $EntriesTemp['FExchangeRate ']='1';//汇率
    $EntriesTemp['FQuantity ']='1';//数量
    $EntriesTemp['FUnitPrice ']='1';//单价

$Voucher->Entries[] = $EntriesTemp;

$Voucher->make();//组装凭证

if($Voucher->status == false){
    echo $Voucher->msg;
}
echo "</br>";

var_dump(json_encode($Voucher->son));

