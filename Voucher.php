<?php

/* code utf-8
 * 用于生成 json 字符串，用于生成k3凭证
 * 
 */

{
    $EntriesTemp = array();//凭证分录明细数组
    $EntriesTemp['FEntryID']='';//分录ID
    $EntriesTemp['FAccountName'] = '';//科目名称  true
    $EntriesTemp['FAccountNumber ']='';//科目代码 true
    $EntriesTemp['FAmount ']='';//本位币金额 true
    $EntriesTemp['FAmountFor ']='';//原币金额 ture;
    $EntriesTemp['FCurrencyName ']=''; //币别名称 ture
    $EntriesTemp['FCurrencyNumber ']='';//币别编码 true
    $EntriesTemp['FDC ']='';//借贷方向 1:借方 0:贷方 true
    $EntriesTemp['FExchangeRate ']='';//汇率
    $EntriesTemp['FQuantity ']='';//数量
    $EntriesTemp['FUnitPrice ']='单价';//单价
    //可为空值
    $EntriesTemp['FExplanation ']='';//备注
    $EntriesTemp['FMeasureUnit ']='';//单位
    $EntriesTemp['FMeasureUnitUUID']='';//单位UUID
    $EntriesTemp['FSettleNo ']='';//结算号
    $EntriesTemp['FSettleTypeName ']='';//结算类型
    $EntriesTemp['FTransNo '] = '业务号';//
    
    $Entries = array();//凭证分录 二维数组
    
    /*现金流量表
    $CashFlowTemp = array();
    $CashFlowTemp['FEntryid']='';//分录ID
    $CashFlowTemp['FEntryid2']='';//对方科目分录ID
    $CashFlowTemp['FAmount ']='';//本位币金额
    $CashFlowTemp['FAmountFor']='';//原币金额
    $CashFlowTemp['FAccNumber']='';//科目编码
    $CashFlowTemp['FAccName']='';//科目名称
    $CashFlowTemp['FItemNumber']='';//主表核算项目编码  
    $CashFlowTemp['FItemName']='';//主表核算项目名称
    $CashFlowTemp['FClassNumber']='';//主表核算项目类型编码
    $CashFlowTemp['FClassName']='';//主表核算项目类型名称
    $CashFlowTemp['FSubItemNumber ']='';//附表核算项目编码
    $CashFlowTemp['FSubItemName']='';//附表核算项目名称
    $CashFlowTemp['FSubClassNumber']='';//附表核算项目类型编码
    $CashFlowTemp['FSubClassName']='';//附表核算项目类型名称
    $CashFlowTemp['FCurrencyNumber']='';//币别编码     
    $CashFlowTemp['FCurrencyName']='';//币别名称
    
    $CashFlow = array();//现今流量表数据 二维数组
     * 
     */
    
    $Voucher = array(); //凭证主体
    $VoucherData = array();//凭证主体数据
    
    $VoucherData['Entries'] = $Entries;//分录 二维数组
    $VoucherData['CashFlow'] = $CashFlow;//现金流量表 二维数组
    $VoucherData['FAttachments'] = '';//附件张数 ture 必录
    $VoucherData['FCashier'] = '';//出纳 
    $VoucherData['FDate'] = '';//凭证日期 ture
    $VoucherData['FExplanation'] = '';//备注
    $VoucherData['FGroup'] = '';//凭证字 ture
    $VoucherData['FHandler'] = '';//会计主管
    $VoucherData['FNumber'] = '';//凭证号 int  ture 
    $VoucherData['FPeriod'] = '';// 会计期间 int  ture 
    $VoucherData['FPoster'] = '';//记账人
    $VoucherData['FPreparer '] = '';//制单人 true
    $VoucherData['FSerialNum'] = '';//凭证序号 
    $VoucherData['FReference'] = '';//参考信息
    $VoucherData['FTransDate'] = '';//业务日期  true
    $VoucherData['FVoucherID'] = '0';//凭证Id 新增传0 修改传凭证Id ture
    $VoucherData['FYear'] = '';//会计年度 ture
    
    //凭证组装完成
    $Voucher['Replace'] = 'false';//只新增不修改
    $Voucher['VoucherData'] = $VoucherData;
    
}

echo json_encode($Voucher);